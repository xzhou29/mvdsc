# MVDSC (Multisource for Vulnerability Detection in Source Code - C/C++)
 - MVDSC: a dataset with graph-based modalities and sequence-based modalities
 - MVDSC-Mixed: with 10,000 additional augmented samples with noisy statments.


## dataset structure:
 - ASTMiner-based: 
 - Data-Flow-based: 
 - Sequence-based:
 - Extracted-features: 

## MVDSC
 - 30000/7500/7500 (train/valid/test)

## MVDSC-Mixed
 - 38000/8500/8500 (train/valid/test)

## Tokenizer:
 - TBA

## Dataloader:
 - TBA
